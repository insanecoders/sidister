

#include <string.h>
#include <stdio.h>
#include "aiff.h"
#include "sidister.h"
#include "songparser.h"

#define V_PRINTF(...) if(verbose){fprintf(stderr, __VA_ARGS__);}
#define CHECK_AND_FREE(x) if(x != NULL) { free(x); x = NULL;}

static uint16_t max_time = 5;
static char *file_name = NULL;
static char *song_file = NULL;
static int8_t *sample_data = NULL;
static bool verbose = false;


static void usage(const char *argv0) {
    fprintf(stderr, "Usage: %s [-t <timelimit in s> -o <output file> -i <song file>\n]", argv0);
}

static bool collect_arguments(int argc, char **argv) 
{
    char ch;
    while ((ch = getopt(argc, argv, "o:t:i:v")) != -1) 
    {
        switch (ch) {
            case 'i':
                song_file = strdup(optarg);
                break;

            case 'o':
               file_name = strdup(optarg);
               break;

            case 't':
               max_time = atoi(optarg);
               if(max_time < 1 || max_time > 1200) {
                   fprintf(stderr, "ERROR: time limit must be > 0 and < 1200 seconds\n");
                   return false;
               }
               break;

            case 'v':
                verbose = true;
                break;

            case '?':
            default:
                return false;
        }
    }
    return (song_file != NULL) && (max_time > 0);
}

void my_at_exit() {
    CHECK_AND_FREE(file_name);
    CHECK_AND_FREE(song_file);
    CHECK_AND_FREE(sample_data);
}

int main(int argc, char **argv)
{
    int8_t *sample_data_ptr;
    song_t *song;

    if(atexit(my_at_exit) != 0) {
        fprintf(stderr, "FATAL: Could not register atexit handler\n");
    }

    if(!collect_arguments(argc, argv)) {
        usage(argv[0]);
        exit(1);
    }

    if(file_name != NULL) {
        // For now, the AIFF writer does not support streaming, 
        // so we allocate memory for entire result.
        size_t  alloc_size = (max_time * 
                              (MIX_MODE == MONO_8BIT ? 1 : 2) *
                              OUTPUT_FREQ
                             );

        sample_data = malloc(alloc_size);
        if(sample_data == NULL) {
            fprintf(stderr, "FATAL: Failed to allocate %zu bytes of memory. Giving up.", alloc_size);
            exit(2);
        }
        sample_data_ptr = sample_data;
    }

    V_PRINTF("INFO:max_time:set:%d\n", max_time);
    V_PRINTF("INFO:output:%s:%s\n", (sample_data ? "file" : "stdout"), (sample_data ? file_name : ""));


    song = read_song_file(song_file);

    if(song == NULL) {
        fprintf(stderr, "FATAL: Could not read song file %s\n", song_file);
        exit(2);
    }
    sidister_init(song);
    do {
        sidister_play(song);
        if(sample_data != NULL) {
            *sample_data_ptr++ = song->sample;
        } else {
            fputc(song->sample, stdout);
        }
    } while(song->tick < (uint32_t)(max_time*OUTPUT_FREQ));

    // Write output file if requested
    if(file_name != NULL && sample_data != NULL) {
        aiff_write(file_name, 
                   (MIX_MODE == MONO_8BIT ? 1 : 2),
                   max_time*OUTPUT_FREQ, 
                   8,
                   OUTPUT_FREQ,
                   sample_data);
    }


    return 0;
}

