
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <stdbool.h>

#define fput_uint16(x, fp) fputc(((x)>>8) & 0xFF, fp); fputc((x) & 0xFF, fp);
#define fput_uint32(x, fp) fput_uint16((uint16_t)((x)>>16), fp); fput_uint16((uint16_t)((x)&0xFFFF), fp);

static const char *AIFF_APPL_CHUNK_STR = "@SID v0.1 by orIgo / iNSANE^C-Lous";

static const int AIFF_NUM_CHUNKS = 3;           // APPL, COMM and SSND


/**
 * Writes a sample as an AIFF file
 */
bool aiff_write(char     *fname, 
                uint8_t  num_channels, 
                uint32_t num_frames, 
                uint16_t sample_size, 
                uint32_t sample_rate, 
                int8_t   *data);
