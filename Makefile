
TARGET=sidister
TARGET_SUFFIX=
DEP_OBJS=
BUILD_TYPE ?= 

include ./deps.mk
include ./mkvars.mk

.PHONY: clean

all: $(TARGET)$(TARGET_SUFFIX)

$(TARGET)$(TARGET_SUFFIX): $(DEP_OBJS)
	$(LD) $(LDOPTS) -o $@$(TARGET_SUFFIX) $(DEP_OBJS)

%.o: %.c
	$(CC) $(COPTS) -o $@ -c $<

%.c: %.h

clean:
	rm -f $(TARGET)$(TARGET_SUFFIX) $(TARGET).o $(DEP_OBJS)