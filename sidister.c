/**
 * SLS => SID-Like Simulator
 *
 * Author: orIgo/iNSANE <origo@amigascene.se>
 * (C) orIgo/iNSANE^C-Lous 2016-12-12
 */

// TODO: Ringmodulation
// TODO: Scramble noise
// TODO: Filters
// TODO: Loop counter

#include <stdio.h>
#include "sidister.h"

/**
 * Pink noise filter derived from the original 'economy version'
 * implementation by paul.kellett@maxim.abel.co.uk in 2000.
 * This version uses fixed precision math only.
 **/
static int8_t pink_noise(uint8_t phase)
{
    static uint32_t b0=127,b1=127,b2=-127;
    uint16_t pink, white = (rand() * phase) % 127;

#define P_B0_CONST1 ((uint16_t)(512*0.99765))           // 5121 (5120.7968)
#define P_B0_CONST2 ((uint16_t)(512*0.0990460))
#define P_B1_CONST1 ((uint16_t)(512*0.96300))
#define P_B1_CONST2 ((uint16_t)(512*0.2965164))
#define P_B2_CONST1 ((uint16_t)(512*0.57000))
#define P_B2_CONST2 ((uint16_t)(512*1.0526913))         //  539 ( 538.9779456)
#define P_WHITE_CONST ((uint16_t)(512*0.1848))          //   95 (  94.6176
    b0 = ((P_B0_CONST1 * b0) + (white * P_B0_CONST2))>>9;
    b1 = ((P_B1_CONST1 * b1) + (white * P_B1_CONST2))>>9;
    b2 = ((P_B2_CONST1 * b2) + (white * P_B2_CONST2))>>9;
    pink = b0 + b1 + b2 + ((white * P_WHITE_CONST)>>9);

    return pink;
}

static int8_t get_wave_phase(const voice_t *v, const uint8_t phase) 
{
    uint8_t wf = v->wf;
    if(!v->enabled) {
        #ifdef DEBUG
        fprintf(stderr, "voice disabled so no wave\n");
        #endif
        return 0;
    }
    
    #ifdef DEBUG
    fprintf(stderr, "get_wave_phase:phase=%d, fco=%d\n", phase, v->pwfco.fco);
    #endif
    
    if(wf == WAVE_SQUARE>>SHIFT_WAVE) {
        return WAVE_VAL_SQUARE(phase, v->pwfco.fco);
    }
    else if(wf == WAVE_TRIANGLE>>SHIFT_WAVE) {
        return WAVE_VAL_TRIANGLE(phase);
    }
    else if(wf == WAVE_SAWTOOTH>>SHIFT_WAVE) {
        return PHASE_CVAL(phase);
    }
    else if(wf == WAVE_NOISE>>SHIFT_WAVE) {
        return pink_noise(phase);
    }

    return 0;
}


static void tick_envelope(envelope_t *e) 
{
    int32_t camp = e->amp;
    if(e->gate) 
    {
        #ifdef DEBUG
        fprintf(stderr, "DEBUG:envel:GATE:ON:amp=%d\n", camp);
        #endif
        
        if(e->flag) 
        {
            #ifdef DEBUG
            fprintf(stderr, "DEBUG:envel:ATTACK:amp=%d a=%d\n", camp, e->adsr.a);
            #endif
            /* attack, until max*/
            camp += e->adsr.a;
            if(camp >= ENVEL_AMP_MAX) 
            {
                camp = ENVEL_AMP_MAX;
                e->flag = false;
            }
        }
        else {
            /* decay, until sustain */
            if(camp > e->adsr.s) 
            {
                #ifdef DEBUG
                fprintf(stderr, "DEBUG:envel:DECAY:amp=%d d=%d s=%d\n", camp, e->adsr.d, e->adsr.s);
                #endif
                camp -= e->adsr.d;
                if(camp < e->adsr.s) 
                {
                    camp = e->adsr.s;
                }
            }
            #ifdef DEBUG
            else {
                fprintf(stderr, "DEBUG:envel:SUSTAIN:amp=%d s=%d\n", camp, e->adsr.s);
            }
            #endif
        }
    } 
    else {
        #ifdef DEBUG
        fprintf(stderr, "DEBUG:envel:GATE:OFF:amp=%d\n", camp);
        #endif
        e->flag = true;

        /* release until 0 */
        if(camp > 0)
        {
            #ifdef DEBUG
            fprintf(stderr, "DEBUG:envel:RELEASE:amp=%d r=%d\n", camp, e->adsr.r);
            #endif
            camp -= e->adsr.r;
            if(camp < 0) 
            {
                camp = 0;
            }
        }
    }
    e->amp = camp;
}

static bool tick_song(song_t *s) 
{
    uint8_t i;

    #ifdef DEBUG
    fprintf(stderr, "DEBUG:song:tick:%d\n", s->tick);
    #endif

#define S_FAC             (ENVEL_AMP_MAX >> 4)
#define CALC_SUSTAIN(s)   ((S_FAC*s))
#define CALC_ATTACK(a)    (ENVEL_AMP_MAX / attack_tab[a])
#define CALC_DECAY(d,s)   ((ENVEL_AMP_MAX - s) / release_tab[d])
#define CALC_RELEASE(r,s) (s / release_tab[r])
    for(i=0; i < NUM_VOICES; i++) 
    {
        voice_t *v = &s->voices[i];
        uint8_t *patdata;

        if(
           s->pats[i] == NULL ||
           s->pats[i]->data == NULL) {
            v->enabled = false;
        }

        if(!v->enabled || 
           v->ppos >= s->pats[i]->len) 
        {
            continue;
        }
        patdata = s->pats[i]->data;

        if(v->pt > 0) 
        {
            /* In pause. Decrease and do next voice */
            v->pt--;
        }
        else {
            bool done = false; /* Set to true when PAUSE or LOOP is encountered*/
            do {
                /* Get next cmd from pat */
                uint8_t cmd = patdata[v->ppos++];
                uint8_t rest = cmd >> NBITS_CMD;
                uint16_t fco;
                #ifdef DEBUG
                fprintf(stderr, "DEBUG:song:v%d:ppos=%d,byte=0x%02x, cmd=0x%02x rest=0x%02x\n", i, v->ppos, cmd, cmd&MASK_CMD, rest);
                #endif
                switch(cmd & MASK_CMD) 
                {
                    case CMD_PAUSE2:
                        v->pt = (uint16_t)(patdata[v->ppos++]);
                        #ifdef DEBUG
                        fprintf(stderr, "DEBUG:song:v%d:set:pt=%d(%dms)\n", i, v->pt, (v->pt*TICK_PER_MS/10), v->pt);
                        #endif
                        v->pt = (v->pt*TICK_PER_MS);
                        done = true;
                        break;

                    case CMD_PAUSE1:
                        #ifdef DEBUG
                        fprintf(stderr, "DEBUG:song:v%d:set:pt=%d(%dms)\n", i, rest, (rest*TICK_PER_MS/10), v->pt);
                        #endif
                        v->pt = (rest*TICK_PER_MS);
                        done = true;
                        break;
                        
                    case CMD_GATE:
                        v->envelope.gate = rest != 0;
                        #ifdef DEBUG
                        fprintf(stderr, "DEBUG:song:v%d:set:ga=%d\n", i, v->envelope.gate);
                        #endif
                        break;

                    case CMD_WAVE:
                        fco = patdata[v->ppos++];
                        fco |= (patdata[v->ppos++]<<8);
                        if(rest == WAVE_SQUARE>>SHIFT_WAVE) { /* SQUARE has PW arg*/
                            v->pwfco.pw = F_COEF(fco)>>1;
                        } else {
                            v->pwfco.fco = F_COEF(fco);//F_COEF(cmd);
                        }

                        /* Re-set envelope if wf changed */
                        if(v->wf != rest) {
                            v->wf = rest;

                            v->envelope.flag = true; /* also set when gate is opened/set */
                            v->envelope.amp = 0;     /* guess so.. */
                            /*v->envelope.phase = 0; */  /* ..not so sure */
                            #ifdef DEBUG
                            fprintf(stderr, "DEBUG:song:v%d:set:wf=0x%02x ga=%d pw=0x%02x\n", i, v->wf, v->envelope.gate, v->pwfco.pw);
                            #endif
                        } else {
                            #ifdef DEBUG
                            fprintf(stderr, "DEBUG:song:v%d:update:wf=0x%02x ga=%d pw=0x%02x\n", i, v->wf, v->envelope.gate, v->pwfco.pw);
                            #endif
                        }
                        break;

                    case CMD_ADSR:
                        v->envelope.flag = true;
                        v->envelope.amp = 0;

                        /* NOTE: Could be optimized if order of ADSR was changed.. */
                        v->envelope.adsr.r = patdata[v->ppos++];
                        v->envelope.adsr.s = patdata[v->ppos++] * S_FAC;
                        v->envelope.adsr.d = patdata[v->ppos++];
                        v->envelope.adsr.a = patdata[v->ppos++];

                        v->envelope.adsr.r = CALC_RELEASE(v->envelope.adsr.r, v->envelope.adsr.s);
                        v->envelope.adsr.a = CALC_ATTACK(v->envelope.adsr.a);
                        v->envelope.adsr.d = CALC_DECAY(v->envelope.adsr.d, v->envelope.adsr.s);

                        #ifdef DEBUG
                        fprintf(stderr, "DEBUG:adsrin=0x%04x %04x %04x %04x, adsr=0x%04x %04x %04x %04x\n",
                                patdata[v->ppos-4],patdata[v->ppos-3],patdata[v->ppos-2],patdata[v->ppos-1],
                                v->envelope.adsr.a, v->envelope.adsr.d, v->envelope.adsr.s, v->envelope.adsr.r);
                        #endif
                        break;

                    case CMD_LOOP:
                        done = true;
                        /* TODO: rest = # of loops */
                        v->ppos = patdata[v->ppos];
                        #ifdef DEBUG
                        fprintf(stderr, "DEBUG:jump=0x%02x\n", v->ppos);
                        #endif
                        break;
                }
            } while(!done);
        }

        /* Voice hopefully set up, tick envelope */
        if((s->tick % TICK_PER_MS) == 0) {
            tick_envelope(&v->envelope);
        }
    }

    s->tick++;
    return true;
}

/* Applies the curent envelope to the voice */
static int16_t apply_envelope(voice_t *v) {
    envelope_t *e = &v->envelope;
    uint16_t tp = e->phase;

    if(v->wf == WAVE_RINGMOD>>SHIFT_WAVE) {
        /* TODO: .. introduce e->rm flag? */
    }

    e->phase += v->pwfco.fco;

    return (get_wave_phase(v, tp>>7) * e->amp>>8);
}



#define COMPRESS_MAX   147
#define COMPRESS_LIMIT 120
#define COMPRESS_FACT  21

static int32_t compress_sample(int32_t sample)
{
    if(sample > COMPRESS_MAX) {
        sample = COMPRESS_LIMIT;
    } 
    else if(sample < -COMPRESS_MAX) {
        sample = -COMPRESS_LIMIT;
    } else {
        int16_t compr = (((((sample*sample)>>7) * sample)>>7)*COMPRESS_FACT)>>7;
        //fprintf(stderr, "sample:%d,compr:%d\n", sample, compr)
        sample = ( (COMPRESS_MAX*sample));
        if(sample > 0) {
            sample -= compr;
        } else{
            sample += compr;
        }
        sample >>= 7;
    }

    if(sample > COMPRESS_LIMIT) {
        sample = COMPRESS_LIMIT;
    } else if(sample < -COMPRESS_LIMIT) {
        sample = -COMPRESS_LIMIT;
    }
    return sample;
}


#if MIX_MODE == STEREO_8BIT
/* Mixes 2 voices into one 8-bit sample */
static int8_t mix_voices(voice_t *v1, voice_t *v2)
{
    if(v1->enabled && v2->enabled) {
        return (apply_envelope(v1) + apply_envelope(v2))>>1;
    } else if(v1->enabled || v2->enabled) {
        return apply_envelope((v1->enabled ? v1 : v2));
    }
    return 0;
}

/* Mixes even voices into a left, and odd into a right channel sample */
static int16_t mix_stereo(song_t *s) 
{
    int8_t lr[2]={0,0};
    uint8_t i=0;
    voice_t *v1, *v2;
    do {
        v1 = &s->voices[i*2];
        v2 = &s->voices[i*2+1];
        lr[i&1] = mix_voices(v1,v2);
    } while(++i <= NUM_VOICES);
#ifdef DEBUG
    fprintf(stderr, "DEBUG:mixed:%d\n", ((((int16_t)lr[0])<<8) | lr[1]));
#endif

    return ((((int16_t)lr[0])<<8) | lr[1]);
}


#define MIX(s) mix_stereo(s)

#else

/* Mixes all voices into a single mono sample (optimized over mix_voices) */
static int8_t mix_mono(song_t *s) 
{
    int32_t sample = 0;
    uint8_t i=0, n=1;
    voice_t *v;
    do {
        v = &s->voices[i];
        if(v->enabled) {
            int16_t mv = apply_envelope(v);
#ifdef DEBUG
    fprintf(stderr, "DEBUG:mix:%d:%04x:%d\n", i, sample, mv);
#endif
            sample += mv;
            n++;
        }
    } while(++i < NUM_VOICES);

    sample /= n;
    sample >>= 6;
    sample = compress_sample(sample);

/*    if(sample >= 127) {
        sample = 127;
    } else if(sample < -127) {
        sample = -127;
    }*/
//    if(n > 1) {
//        sample >>= (n-1);
//    }
#ifdef DEBUG
    fprintf(stderr, "DEBUG:mixed:%d\n", sample);
#endif

    return (int8_t)sample&0xff;
}

#define MIX(s) mix_mono(s)

#endif

void sidister_init(song_t *s) 
{
    s->tick = 0;
}

void sidister_play(song_t *s) 
{
    if(tick_song(s)) {
        s->sample = MIX(s);
    }
}
