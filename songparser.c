
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <assert.h>
#include "songparser.h"

typedef enum _parse_state_t {
    NONE,
    INSTRUMENT,
    SONG, 
    END
} parse_state_t;

typedef struct _parse_pattern_t {
    struct _pattern_t;
    uint16_t buflen;
} parse_pattern_t;

/* How much to allocate extra for every instrument buffer realloc.
 * A 'good value' depends on malloc impl and performance/mem requirements
 */
#define INSTR_BUF_INCR  0
static uint16_t sp_num_instruments = 0;

/* Build uint32 from 4 bytes. Used to avoid -Wfour-char-constant */
#define INSTR_TAG(a,b,c,d) ((a<<24) | (b<<16) | (c<<8) | d)

#define NUM_NOTES 12
static const char note_idx[NUM_NOTES] = {
    'C','c','D','d','E','F','f','G','g','A','a','B'
};

// There are 8 octaves in total ranging from C0 (274) to B7 (65288).
// The value of one note at one octave is exactly half of the octave above.
#define MAX_OCTAVE 7
#define NUM_OCTAVES (MAX_OCTAVE+1)
#define BASE_OCTAVE 6           // gives just a tiny bit of error low octave 0 and 1..
#define NOTE_VAL(o, n) (((((n<<8)>>BASE_OCTAVE)<<o)>>8) & 0xFFFF)
#define MAKE_OCTAVE(o) {NOTE_VAL(o, 17557), NOTE_VAL(o, 18601),NOTE_VAL(o, 19709),NOTE_VAL(o, 20897), \
NOTE_VAL(o, 22121),NOTE_VAL(o, 23436),NOTE_VAL(o, 24830),NOTE_VAL(o, 26306), \
NOTE_VAL(o, 27871),NOTE_VAL(o, 29528),NOTE_VAL(o, 31234),NOTE_VAL(o, 33144)}
static const uint16_t note_tab[NUM_OCTAVES][NUM_NOTES] = {
    MAKE_OCTAVE(0),
    MAKE_OCTAVE(1),
    MAKE_OCTAVE(2),
    MAKE_OCTAVE(3),
    MAKE_OCTAVE(4),
    MAKE_OCTAVE(5),
    MAKE_OCTAVE(6),
    MAKE_OCTAVE(7)
};


// TODO: Translate note_tab to actual fcoef
static uint16_t instr_parse_note (char *p)
{
    uint8_t i;
    uint16_t n = -1;
    char c = p[0];  // note, followed by octave
    int8_t o = (p[1]-'0');  // Octave

    if(o < 0 && o > MAX_OCTAVE) {
        fprintf(stderr, "WARNING: Invalid octave '%d'\n", o);
        o = 0;
    }

    for(i=0; i < NUM_NOTES; i++) {
        if(c == note_idx[i]) {
            return note_tab[o][i];
        }
    }

    fprintf(stderr, "WARNING: Note value not found for '%s', assuming int\n", p);
    return atoi(p);
}

// @return uint8_t The number of bytes used for a cmd
static uint8_t instr_cmd_parse(uint32_t tag, uint8_t *cmd, char *argptr, uint32_t *args)
{
    uint32_t t;
    *cmd = CMD_WAVE;  // common case?

    switch(tag) {
        case INSTR_TAG('G','A','T','E'):
            *cmd = CMD_GATE | (*argptr == '0' ? 0x00 : 0x10);
            fprintf(stderr, "GATE:cmd=0x%02x\n", *cmd);
            return 1; // only cmd

        case INSTR_TAG('W','A','V','E'): // wf in up nibble, and 2 args, hi8,lo8=note
            t = *argptr; // WF
            fprintf(stderr, "WAVE:%c\n", t&0x7f);
            switch(t&0xff) {  // MAY NOT be A-F, because they are 4 HEX args
                case 'R': //Rectangle, S is for Sawtooth
                case 'P':
                    t = WAVE_SQUARE;
                    break;
                case 'S':
                case 'W':
                    t = WAVE_SAWTOOTH;
                    break;
                case 'T':
                    t = WAVE_TRIANGLE;
                    break;
                case 'N':
                    t = WAVE_NOISE;
                    break;
                case 'Q':
                    t = WAVE_QUIET;
                    break;
                default:
                    fprintf(stderr, "Unknown wave type: %c, argptr: %s", 
                            t&0xff, argptr);
                    exit(3);
            }
            *cmd |= t;
            *args = instr_parse_note(argptr+2);
            fprintf(stderr, "WAVE:cmd=0x%2x, args=%0x08x\n", *cmd, *args);
            return 3; // cmd + 16 bit arg

        case INSTR_TAG('P','A','U','S'):
            if(*args > 15) {
                *cmd = CMD_PAUSE2;
                #ifdef DEBUG
                fprintf(stderr, "PAUSE:2:cmd=0x%02x,args=0x%08x\n", *cmd, *args);
                #endif
                return 2; // cmd + 1 arg
            }
            *cmd = CMD_PAUSE1|((*args&0xff)<<NBITS_CMD);
            #ifdef DEBUG
            fprintf(stderr, "PAUSE:1:cmd=0x%02x,args=0x%08x\n", *cmd, *args);
            #endif
            return 1; // Only cmd

        case INSTR_TAG('L','O','O','P'):
            *cmd = CMD_LOOP;
            return 2; // cmd + 1 arg

        case INSTR_TAG('A','D','S','R'):
            *cmd = CMD_ADSR;
            fprintf(stderr, "ADSR:cmd=0x%02x,args=0x%08x\n", *cmd, *args);
            return 5; // cmd + 4 args

        default:
            fprintf(stderr, "ERROR: Unknown cmd tag: %08x (%c%c%c%c)", 
                    tag,
                   (tag>>24) & 0xff,
                   (tag>>16) & 0xff,
                   (tag>>8) & 0xff,
                   tag & 0xff);
            exit(3);
    }
}

// @return NULL if short on memory or ptr to voice pattern
parse_pattern_t *get_parse_pattern(song_t *s, uint8_t voice)
{
    parse_pattern_t *p = (parse_pattern_t *)s->pats[voice];

    if(p == NULL) {
        p = malloc(sizeof(parse_pattern_t));
        if(p == NULL) {
            fprintf(stderr, "ERROR: Could not allocate %zu bytes for pattern\n",
                    sizeof(parse_pattern_t));
            return NULL;
        }
        s->pats[voice] = (pattern_t *)p;
        p->len = p->buflen = 0;
        p->data = NULL;
    }
    return p;
}

// Makes sure there is enough space allocated in a pattern to 
// fit another 'needed' number of bytes.
// @return bool true if successful, false if short on memory
bool ensure_pattern_space(parse_pattern_t *p, uint16_t more)
{
    uint16_t needed = p->len + more;
    #ifdef DEBUF
    fprintf(stderr, "DEBUG:ensure_pattern_space:have=%d, need:%d\n", p->buflen, needed);
    #endif
    if(p->data == NULL || 
       needed >= p->buflen) {
        uint8_t *newptr = realloc(p->data, needed);
        if(newptr == NULL) {
            fprintf(stderr, "ERROR: Could not allocate %d more bytes for pattern data (for total %d bytes)\n",
                    more, needed);
            if(p->data) {
                free(p->data);
            }
            p->data = NULL;
            return false;
        }
        p->data = newptr;
        p->buflen = needed;
        #ifdef DEBUG
        fprintf(stderr, "DEBUG: (re)-allocated %d of pattern buf\n", p->buflen);
        #endif
    }
    return true;
}

// Add a single command to a specified voice pattern.
// @return bool true if successful, false if short on memory
static bool add_cmd_to_voice_pat (song_t *song,
                                  uint8_t voice, 
                                  uint8_t cmd,
                                  uint8_t cmd_len,        // 1-5
                                  uint32_t arg)
{
    parse_pattern_t *p = get_parse_pattern(song, voice);
    uint8_t i;
    if(!ensure_pattern_space(p, cmd_len+1)) {
        return false;
    }
    p->data[p->len++] = cmd;
    for(i=0; i < cmd_len-1; i++) {
        #ifdef DEBUG
        fprintf(stderr, "DEBUG: Adding cmd of length %d to voicepattern %d, which is %d long\n", cmd_len, voice, p->len);
        #endif
        p->data[p->len++] = (uint8_t)(arg & 0xff);
        arg >>= 8;
    }
    return true;
}

// Add an instrument to play at the end of the current pattern buffer
// for a voice. (Re)allocates space and copies instrument definition.
// @return bool true if succesful
bool add_instrument_to_voice_pat(song_t *song, uint8_t voice, instrument_t *instr) 
{
    parse_pattern_t *p = get_parse_pattern(song, voice);
    uint8_t i;

    if(!p) {
        fprintf(stderr, "ERROR: Could not get pattern for voice %d\n", voice);
        return false;
    }
    if(!ensure_pattern_space(p, instr->len)) {
        return false;
    }

    for(i=0; i < instr->len; i++) {
        #ifdef DEBUG
        fprintf(stderr, "DEBUG: Adding instrument of length %d to voicepattern %d, which is %d long: 0x%02x\n", instr->len, voice, p->len, instr->data[i]);
        #endif
        p->data[p->len++] = instr->data[i];
    }
    #ifdef DEBUG
    fprintf(stderr, "DEBUG: Added instrument of length %d to voicepattern %d, which is now %d long and first byte 0x%02x\n", instr->len, voice, p->len, instr->data[0]);
    #endif

    return true;
}

// Parses a single line voice line belonging to a SONG block
// @return bool true if parsing was succesful
static bool parse_voice_line (song_t *song, char *line)
{
    instrument_t *instr;
    uint8_t v = line[1]-'0';
    uint16_t instr_no = atoi(line+3);
    uint8_t pos2 = instr_no<10 ? 4 : (instr_no<100 ? 5 : 6); // where to find n_times
    uint16_t n_times = 1;

    #ifdef DEBUG
    fprintf(stderr, "DEBUG:voice:%d:instr:%d\n", v, instr_no);
    #endif

    if(v < 1 || v > NUM_VOICES) {
        fprintf(stderr, "ERROR: Invalid voice # '%d'\n", v);
        return false;
    }
    v--;
    
    if(line[2] != ':') {
        fprintf(stderr, "ERROR: Invalid format, expecting : after 'V%d'\n", v);
        return false;
    }

    if(pos2 >= strlen(line) || line[pos2] != ',') {
        fprintf(stderr, "WARNING: parse error, missing ',', assuming 1 repeat (pos2=%d, line=%s)\n", pos2, line);
        // if(strict_parsing) {       return false;}
    } else {
        n_times = atoi(line+pos2+1);
    }

    if(instr_no < 1 || instr_no > MAX_INSTRUMENTS) {
        fprintf(stderr, "ERROR: instrument # invalid: %d'n", instr_no);
        return false;
    }

    if(n_times < 1 || n_times > MAX_LOOPS) {
        fprintf(stderr, "ERROR: loop count invalid: %d\n", n_times);
        return false;
    }
    

    if(instr_no > sp_num_instruments) {
        fprintf(stderr, "ERROR: instrument id %d out of range (ma %d)", instr_no,
               sp_num_instruments);
        return false;
    }
    instr_no--; // instruments are 1-indexed
    fprintf(stderr, "DEBUG: instr_no:%d", instr_no);
    instr = song->instruments[instr_no];
    do {
        add_instrument_to_voice_pat(song, v, instr);
    } while((--n_times) > 0);

    return true;
}

// Parses a single line belonging to an INSTRUMENT block
// @return bool true if parsing was successul.
static bool parse_instrument_line (song_t *song, char *line) 
{
    instrument_t *inst = song->instruments[sp_num_instruments];
    const uint32_t tag = INSTR_TAG(line[0], line[1], line[2], line[3]);
    uint32_t arg = 0;
    uint8_t cmd, cmd_len;
    char *argptr1, *argptr = line;

    if(inst == NULL) {
        inst = (instrument_t *)calloc(sizeof(instrument_t), 1);
        song->instruments[sp_num_instruments] = inst;
    }
    
    while(*argptr != ':' && *argptr++ != 0x00) {}

    if(*argptr == 0x00) {
        fprintf(stderr, "ERROR: Parse error\n");
        return false;
    }
    //argptr++; // skip ':'

    argptr1 = argptr+1; // save ptr to start of args (after :)

    // Stash up to 4 byte-args into a uint32
    // If there are be more than 4 args, 'arg' will be screwed. Oh, well.
    while((*argptr != 0x00) && (*argptr+1 != 0x00)) {
        argptr++;
        uint8_t a = *argptr;
        if(*argptr >= 'A' && *argptr <= 'F') { // allow HEX also
            a = *argptr-'A'+10;
        } else if(a >= '0' && a <= '9') {
            a =  (uint8_t)(atoi(argptr) & 0xFF);
        }
        arg = arg<<8 | a;
        while(*argptr != ',' && *argptr++ != 0x00) {}
    }

    cmd_len = instr_cmd_parse(tag, &cmd, argptr1, &arg);

    if(inst->buflen == 0 || inst->buflen <= cmd_len+inst->len) {
        uint16_t newlen = cmd_len + inst->buflen;
        uint8_t *newptr = realloc(inst->data, newlen);

        if(newptr == NULL) {
            fprintf(stderr, "ERROR: Could not allocate %d bytes for instrument data\n", 
                    newlen);
            free(inst->data);
            inst->data = NULL;
            inst->buflen = 0;
            inst->len = 0;
            return false;
        }

        inst->data = newptr;
        inst->buflen = newlen;
        #ifdef DEBUG
        fprintf(stderr, "DEBUG: (re-)allocated %d bytes for instrument data\n", inst->buflen);
        #endif
    }

    inst->data[inst->len++] = cmd;

    while((--cmd_len) > 0) {
        inst->data[inst->len++] = (arg & 0xFF);
        arg >>= 8;
    }

    return true;
}


// Keeps track of parser state and parses a single
// line from the song file.
// @return bool true if parsing was successful
static bool parse_line (song_t *song, char *line) 
{
    static parse_state_t state = NONE;
    if(line[0] == '#' || line[0] == '\n') {
        return true;
    }
    switch(state) {
        case NONE:
            if(strncmp(line, "SONG", 4) == 0) 
            {
                state = SONG;
            } 
            else if(strncmp(line, "INSTR", 5) == 0) 
            {
                state = INSTRUMENT;
            } 
            else if(strncmp(line, "END", 3) == 0) 
            {
                state = END;
            }
            break;

        case INSTRUMENT:
            if(strncmp(line, "END", 3) == 0) {
                state = NONE;
                sp_num_instruments++;
                break;
            }
            return parse_instrument_line(song, line);

        case SONG:
            if(strncmp(line, "END", 3) == 0) {
                state = NONE;
                break;
            }

            if(line[0] == 'V' && line[1]-'0' > 0 && line[1]-'0' <= NUM_VOICES) {
                return parse_voice_line(song, line);
            }
            else if(strncmp(line, "NAME", 4) == 0) {
                song->name = strdup(line+5);
            }
            else if(strncmp(line, "SPEED", 5) == 0) {
                song->speed = atoi(line+6);
                if(song->speed < 1 || song->speed > 100) {
                    fprintf(stderr, 
                            "ERROR: Song speed %d invalid. Must be between 1 and 100\n",
                            song->speed);
                }
            }
            break;

        case END:
            break;
    }

    return true;
}

static void parser_init_song(song_t *s) 
{
    uint8_t i;

    memset(s->instruments, 0, sizeof(instrument_t *) * MAX_INSTRUMENTS);
    memset(s->pats, 0, sizeof(pattern_t *) * NUM_VOICES);
    for(i=0; i < NUM_VOICES; i++) {
        s->voices[i].enabled = true;
        s->voices[i].wf = WAVE_QUIET>>SHIFT_WAVE;
    }
}

#define PARSE_BUF_SIZE 128

song_t *read_song_file (char *fname) 
{
    FILE *fp = fopen(fname, "r");
    song_t *song;
    uint16_t lno = 0, num;
    char buf[PARSE_BUF_SIZE];
    
    if(fp == NULL) {
        fprintf(stderr, "Could not open file %s: %d\n", fname, errno);
        return NULL;
    }

    song = calloc(sizeof(song_t), 1);
    if(song == NULL) {
        fclose(fp);
        fprintf(stderr, "Could not allocate %zu bytes of memory for song\n",
               sizeof(song_t));
        return NULL;
    }
    parser_init_song(song);

    memset((void *)buf, 0, PARSE_BUF_SIZE-1);
    while( (fgets(buf, PARSE_BUF_SIZE-2, fp)) != NULL ) {
        lno++;
        if(!parse_line(song, buf)) {
            fprintf(stderr, "ERROR: Parsing aborted on line %d: '%s'\n", lno, buf);
            break;
        }
        memset(&buf, 0, PARSE_BUF_SIZE-1);
    }

    fclose(fp);
    return song;
}

void free_song (song_t *song)
{
    free(song);
}
