
#include "aiff.h"

// Converts an unsigned long to 80 bit IEEE Standard 754 floating point number.
static void fput_ieee754float(uint32_t value, FILE *fp) 
{
    unsigned long exp = value;
    unsigned char i;
    char buf[2] = {0,0};

    exp = value>>1;
    for (i=0; i<32; i++) { 
        exp>>= 1;
        if (!exp) break;
    }
    buf[0] = 0x40;
    buf[1] = i;

    for (i=32; i; i--)
    {
      if (value & 0x80000000) break;
      value <<= 1; 
    }
//    *(unsigned long *)(&buf+2) = value;
    fwrite(buf, 1, 2, fp);
    fput_uint32(value, fp);
    fput_uint32(0, fp);
}

bool aiff_write(char     *fname, 
                uint8_t  num_channels, 
                uint32_t num_frames, 
                uint16_t sample_size, 
                uint32_t sample_rate, 
                int8_t   *data)
{
    FILE *fp;
    uint32_t num_sample_points = (num_frames*num_channels);
    uint32_t data_size = num_sample_points * (sample_size>>3);
    //long double samp_rate_d = sample_rate;

    uint32_t form_size = 4 + // 'AIFF'
//                         strlen(AIFF_APPL_CHUNK_STR) + 4*2 +   // APPL string + "APPL" + size
                         (26) +   // 'COMM' + chunk size
                         (data_size + 4*4); // 'SSND' + len + offs&bz,  FORM + size is NOT included in form_size
    fprintf(stderr, "DEBUG:aiff_write:num_frames=%u, sample_size=%d, sample_rate=%d, data_size=%u\n", 
            num_frames, sample_size, sample_rate, data_size);
    fp = fopen(fname, "w");
    if(!fp) {
        return false;
    }
    
    fputs("FORM", fp);
    fput_uint32(form_size, fp);

    fputs("AIFF", fp);

    // COMM chunk
    fputs("COMM", fp);
    fput_uint32(0x0012, fp);
    fput_uint16(num_channels, fp);
    fput_uint32(num_frames, fp);
    fput_uint16(sample_size, fp);
    fput_ieee754float(sample_rate, fp);

    // APPL chunk
    /*fputs("APPL", fp);
    fput_uint32(strlen(AIFF_APPL_CHUNK_STR), fp);
    fwrite(AIFF_APPL_CHUNK_STR, 1, strlen(AIFF_APPL_CHUNK_STR), fp);
*/
    // SSND chunk if we have data
    if(num_frames > 0) {
        fputs("SSND", fp);
        fput_uint32(data_size, fp);
        fput_uint32(0, fp);                     // offset, not used
        fput_uint32(0, fp);                     // blockSize, not used
        fwrite(data, 1, data_size, fp);
    }

    fclose(fp);
    
    return true;
}
