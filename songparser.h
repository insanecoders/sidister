#ifndef __SONGPARSER_H__
#define __SONGPARSER_H__

#include "sidister.h"

song_t *read_song_file(char *fname);
void free_song(song_t *song);

#endif
