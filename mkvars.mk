# Default mkvars.mk, with options for "normal" POSIX/gcc compatible
# systems. For a more 'exotic' build, look at mkvars-emscripten.mk

# Modify as necessary

# -fms-extensions are for anonymous structs, used for parse_pattern_t to more nicely 'extend' pattern_t
# If not available, rewrite parse_pattern_t to include the fields of pattern_t.
COPTS ?= -O2 -g0
COPTS  += -fms-extensions -I. -Wall \
         -DNUM_VOICES=4 -DOUTPUT_FREQ=44100
LDOPTS ?= -lc
LD	    = gcc
CC      = gcc


# Add platform specific options

ifeq ($(OS),Windows_NT)
    uname_S := Windows
else
    uname_S := $(shell uname -s)
endif

ifeq ($(uname_S),Windows)
    TARGET_SUFFIX := .exe
    COPTS += -D WINDOWS
endif

ifeq ($(uname_S),Darwin)
    LD	    = ld
    COPTS += -D OSX -Wfour-char-constants
    sw_ver=`sw_vers -productVersion`
    LDOPTS += -macosx_version_min $(sw_ver)
endif

ifeq ($(BUILD_TYPE),)
else
    include ./mkvars$(BUILD_TYPE).mk
endif
