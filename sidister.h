#ifndef __SIDISTER_H__
#define __SIDISTER_H__

#include <stdlib.h>
#include <unistd.h>
#include <inttypes.h>
#include <stdbool.h>

#ifndef NUM_VOICES
#define NUM_VOICES      3               /* Really < INT_MAX .. */
#endif

#ifndef OUTPUT_FREQ
#define OUTPUT_FREQ     (44100)         /* Hz */
#endif

/* Mixing modes */
#define MONO_8BIT   0                   /* PCM mono 8 bit */
#define STEREO_8BIT 1                   /* PCM stereo 8 bit/ch, Even/odd voices on left/right channel */


#ifndef MIX_MODE
#define MIX_MODE    ((NUM_VOICES & 1) == 0 ? STEREO_8BIT : MONO_8BIT)
#endif

#define VOICE1         0x00
#define VOICE2         0x01
#define VOICE3         0x02
#if NUM_VOICES > 3
#define VOICE4         0x03
#endif
#if NUM_VOICES > 4
#define VOICE5         0x04
#endif
#if NUM_VOICES > 5
#define VOICE6         0x05
#endif
#if NUM_VOICES > 6
#define VOICE7         0x06
#endif
#if NUM_VOICES > 7
#define VOICE8         0x07
#endif

// 3 bits for CMD - perhaps make it 4, for convenience? :)
#define SHIFT_CMD        0
#define MASK_CMD         (0x07)<<SHIFT_CMD
#define CMD_WAVE         0x07    // Set waveform, 8 bits pw|fcoef
#define CMD_PAUSE1       0x06    // 4-bit pause in upper 4 bits
#define CMD_PAUSE2       0x02    // 12-bit pause, 4 MSB in cmd, 8 LSB follow
#define CMD_ADSR         0x03    // Set AD, 32 bits flw
#define CMD_LOOP         0x04    // Jump to pos, 8 bits
#define CMD_GATE         0x05    // Set Gate ON/OFF, 8 bits follow
#define CMD_UNUSED2      0x01    // Unused
#define CMD_UNUSED3      0x00    // Unused
#define NBITS_CMD        4

// 3 bits for WAVE type
#define SHIFT_WAVE      NBITS_CMD
#define MASK_WAVE       (0x07<<SHIFT_WAVE)
#define WAVE_NOISE      (0x00<<SHIFT_WAVE)
#define WAVE_SQUARE     (0x01<<SHIFT_WAVE)
#define WAVE_SAWTOOTH   (0x02<<SHIFT_WAVE)
#define WAVE_TRIANGLE   (0x03<<SHIFT_WAVE)
#define WAVE_QUIET      (0x04<<SHIFT_WAVE)
#define WAVE_RINGMOD    (0x05<<SHIFT_WAVE)
#define NBITS_WAVE      3
#define SHIFT_PW_COEF   NBITS_CMD+NBITS_WAVE

#define ENVEL_AMP_MAX (127<<8)

#define TICK_PER_MS (OUTPUT_FREQ/1000)
#define F_COEF(x) (((uint32_t)(x<<10)/OUTPUT_FREQ))

#define MAX_INSTRUMENTS 63
#define MAX_LOOPS       (1<<4)-1        // 15

typedef struct _adsr_t {
    uint16_t a;  // attack rate  (>(ENVEL_AMP_MAX/24000) && < (ENVEL_AMP_MAX/2))
    uint16_t d;  // decay rate
    uint16_t s;  // sustain lvl  (0 - ENVEL_AMP_MAX/2)
    uint16_t r;  // release rate 
} adsr_t;

typedef struct _envelope_t {
    bool flag : 1;
    bool gate : 1;
    adsr_t adsr;
    uint16_t  phase;
    uint16_t  amp;      // is <<8
} envelope_t;

typedef struct _voice_t {
    bool      enabled;
    uint16_t  pt;        // PauseTime
    uint16_t  ppos;       // pattern pos
    uint8_t   wf;         // Waveform
    uint8_t   amp;         // Amplitude
    union {
        uint16_t pw;     // Pulsewidth 0-255 for square
        uint16_t fco;    // Freq coef for phase
    } pwfco;
    envelope_t envelope;
} voice_t;

typedef struct _pattern_t {
    uint8_t *data;
    uint16_t len;
} pattern_t;

typedef struct _instrument_t {
    uint8_t *data;
    uint16_t len;
    uint16_t buflen;
} instrument_t;

typedef struct _song_t {
    uint32_t        tick;
#if MIX_MODE == MONO_8BIT
    int8_t          sample;
#else
    int16_t         sample;
#endif
    uint8_t         speed;
    char           *name;
    voice_t         voices[NUM_VOICES];
    pattern_t      *pats[NUM_VOICES];
    instrument_t   *instruments[MAX_INSTRUMENTS];
} song_t;


/**
 * From 6581 "spec">
 * Attack rate:         2ms - 8s
 * Decay/Release rate:  6ms - 24s
 */
static const uint16_t attack_tab[16] = 
    {   2,   4, 16,  24,  38,  58,  68,  80,
      100, 250, 500,800,1000,3000,5000,8000};
static const uint16_t release_tab[16] = 
    {    6,   24,   48,   72,  114, 168,   204,  240, 
       300,  750, 1500, 2400, 3000, 9000,15000,24000};

#define PHASE_CVAL(phs) ((-128 + phs))
#define WAVE_VAL_SQUARE(phs, pw) (phs > pw ? 127 : -127) 
#define WAVE_VAL_TRIANGLE(phs) ( (phs & 0x80 ? (PHASE_CVAL(phs)<<1)^0xff : PHASE_CVAL(phs)<<1)-128)

void sidister_play(song_t *s);
#endif
